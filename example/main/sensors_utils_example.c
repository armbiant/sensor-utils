/**
 * @file
 * @author Martin Stejskal
 * @brief Main user function - mainly for creating tasks
 */
// ===============================| Includes |================================
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "cfg.h"
#include "sensors.h"
#include "sensors_utils.h"

// ================================| Defines |================================
/**
 * @brief Small pause between tasks
 */
#define SILENCE_FOR_SEC (30)

/**
 * @brief How long raw data will be sampled
 */
#define SAMPLE_RAW_DATA_FOR_SEC (10)

/**
 * @brief How long tap detection should run
 */
#define TAP_DETECTION_FOR_SEC (30)

/**
 * @brief How long orientation tracking should run
 */
#define ORI_TRACKING_FOR_SEC (30)

/**
 * @brief After cycle completetation, sleep for this time in seconds
 */
#define CYCLE_SILENCE_FOR_SEC (50)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef struct {
  te_sensor_feature e_feature_sampling;
} ts_runtime_main;
// ===========================| Global variables |============================
/**
 * @brief Tag for logging system
 */
static char *tag = "main";

/**
 * @brief Runtime variables
 */
static ts_runtime_main ms_runtime_main;
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void _read_sensor_data_task(void *pv_args);
// ==============| Internal function prototypes: middle level |===============
static void _start_sampling_raw_data(void);
static void _stop_sampling_raw_data(void);

static void _start_tap_detection(void);
static void _stop_tap_detection(void);

static void _start_orientation_tracking(void);
static void _stop_orientation_tracking(void);
// ================| Internal function prototypes: low level |================
static void _print_data_sampling_cb(ts_sensor_utils_data *ps_data);
static void _print_tap_detected_cb(void);
static void _print_ori_tracking(bool b_in_target_position);

static void _print_data(ts_sensor_utils_data *ps_data,
                        te_sensor_feature e_feature);
static void _sleep_sec(uint32_t u32_sleep_sec);
// =========================| High level functions |==========================
void app_main(void) {
  ESP_LOGI(tag, "Sensors utilities example alive!");

  // If you need high performance, it is good idea to use separate CPU and
  // increase priority bit (in case other tasks would run there)
  xTaskCreatePinnedToCore(sensors_utils_task, "Sensors utils", 8 * 1024, NULL,
                          tskIDLE_PRIORITY + 10, NULL, PRO_CPU);

  // Better to run application on another core
  xTaskCreatePinnedToCore(_read_sensor_data_task, "App", 4 * 1024, NULL,
                          tskIDLE_PRIORITY, NULL, APP_CPU);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void _read_sensor_data_task(void *pv_args) {
  // Wait until sensor module is ready
  ESP_LOGD(tag, "Waiting for sensor module to be ready");
  while (!sensors_utils_is_initialized()) {
    vTaskDelay(1);
  }

  ESP_LOGI(tag, "Sensor module ready!");

  // Repeat whole program over and over
  while (1) {
    // Start sampling data from accelerometer for few seconds
    _start_sampling_raw_data();
    _sleep_sec(SAMPLE_RAW_DATA_FOR_SEC);
    _stop_sampling_raw_data();

    _sleep_sec(SILENCE_FOR_SEC);

    _start_tap_detection();
    _sleep_sec(TAP_DETECTION_FOR_SEC);
    _stop_tap_detection();

    _sleep_sec(SILENCE_FOR_SEC);

    _start_orientation_tracking();
    _sleep_sec(ORI_TRACKING_FOR_SEC);
    _stop_orientation_tracking();

    _sleep_sec(CYCLE_SILENCE_FOR_SEC);
  }
}
// ===================| Internal functions: middle level |====================
static void _start_sampling_raw_data(void) {
  ts_sensor_util_sampling_args s_args = {.pf_get_data =
                                             _print_data_sampling_cb};
  sensors_utils_get_default_sampling_args(&s_args);

  // Backup feature value - will be used by callback later
  ms_runtime_main.e_feature_sampling = s_args.e_feature;

  e_sensor_error e_err_code = sensors_utils_sampling_start(s_args);

  if (e_err_code) {
    ESP_LOGE(tag, "Started sampling: %s", sensor_error_code_to_str(e_err_code));
  } else {
    ESP_LOGI(tag, "Started sampling");
  }
}
static void _stop_sampling_raw_data(void) {
  e_sensor_error e_err_code = sensors_utils_sampling_stop();

  if (e_err_code) {
    ESP_LOGE(tag, "Stop sampling: %s", sensor_error_code_to_str(e_err_code));
  } else {
    ESP_LOGI(tag, "Stop sampling");
  }
}

static void _start_tap_detection(void) {
  ts_sensor_util_tap_det_args s_args = {.pf_tap_detected =
                                            _print_tap_detected_cb};
  sensors_utils_get_default_tap_detect(&s_args);

  e_sensor_error e_rrr_code = sensors_utils_tap_detection_start(s_args);

  if (e_rrr_code) {
    ESP_LOGE(tag, "Start tap detection: %s",
             sensor_error_code_to_str(e_rrr_code));
  } else {
    ESP_LOGI(tag, "Start tap detection");
  }
}

static void _stop_tap_detection(void) {
  e_sensor_error e_err_code = sensors_utils_tap_detection_stop();

  if (e_err_code) {
    ESP_LOGE(tag, "Stop tap detection: %s",
             sensor_error_code_to_str(e_err_code));
  } else {
    ESP_LOGI(tag, "Stop tap detection");
  }
}

static void _start_orientation_tracking(void) {
  ts_sensor_util_ori_det_args s_args = {.pf_ori_changed = _print_ori_tracking};
  sensors_utils_get_default_ori_tracking(&s_args);

  e_sensor_error e_err_code = sensors_utils_ori_tracking_start(s_args);
  if (e_err_code) {
    ESP_LOGE(tag, "Start orientation tracking: %s",
             sensor_error_code_to_str(e_err_code));
  } else {
    ESP_LOGI(tag, "Start orientation tracking");
  }
}

static void _stop_orientation_tracking(void) {
  e_sensor_error e_err_code = sensors_utils_ori_tracking_stop();

  if (e_err_code) {
    ESP_LOGE(tag, "Stop orientation tracking: %s",
             sensor_error_code_to_str(e_err_code));
  } else {
    ESP_LOGI(tag, "Stop orientation tracking");
  }
}
// =====================| Internal functions: low level |=====================
static void _print_data_sampling_cb(ts_sensor_utils_data *ps_data) {
  // Need to load feature value, in order to be able to print it
  _print_data(ps_data, ms_runtime_main.e_feature_sampling);
}

static void _print_tap_detected_cb(void) {
  ESP_LOGI(tag, "TAP detected (^_^)");
}

static void _print_ori_tracking(bool b_in_target_position) {
  ESP_LOGI(tag, "In position: %d", b_in_target_position);
}

static void _print_data(ts_sensor_utils_data *ps_data,
                        te_sensor_feature e_feature) {
  const te_sensor_data_type e_type = ps_data->e_data_type;

  if ((e_type == SENSOR_DTYPE_RAW) || (e_type == SENSOR_DTYPE_INT)) {
    ESP_LOGI(tag, "Data: %d", ps_data->u_d.i16);

  } else if ((e_type == SENSOR_DTYPE_RAW_VECT) ||
             (e_type == SENSOR_DTYPE_INT_VECT)) {
    ESP_LOGI(tag, "%s : Vector: %d %d %d", sensor_feature_to_str(e_feature),
             ps_data->u_d.s_vect.i16_x, ps_data->u_d.s_vect.i16_y,
             ps_data->u_d.s_vect.i16_z);

  } else if (e_type == SENSOR_DTYPE_FLOAT) {
    ESP_LOGI(tag, "%s : Float data: %f", sensor_feature_to_str(e_feature),
             ps_data->u_d.f);

  } else if (e_type == SENSOR_DTYPE_FLOAT_VECT) {
    ESP_LOGI(tag, "%s : Float vector: %f %f %f",
             sensor_feature_to_str(e_feature), ps_data->u_d.s_vect_float.f_x,
             ps_data->u_d.s_vect_float.f_y, ps_data->u_d.s_vect_float.f_z);
  } else {
    ESP_LOGE(tag, "Unexpected data type: %s (%d)",
             sensor_data_type_to_str(e_type), e_type);
  }
}

static void _sleep_sec(uint32_t u32_sleep_sec) {
  vTaskDelay((u32_sleep_sec * 1000) / portTICK_PERIOD_MS);
}
