/**
 * @file
 * @author Martin Stejskal
 * @brief Utilities for "sensors" module
 */
// ===============================| Includes |================================
#include "sensors_utils.h"

#include <esp_timer.h>
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>
#include <math.h>

#include "math_fast.h"
#include "math_vector.h"
#include "sensors.h"
// ================================| Defines |================================
/**
 * @brief Define relative size of queue
 *
 * In case that processing will take bit more time, data will not be lost if
 * queue size is big enough
 */
#define _QUEUE_NUM_OF_ITEMS (30)

// ==========================| Defines - Sampling |===========================
/**
 * @brief Sampling related
 *
 * @{
 */

/**
 * @brief Default feature for sampling
 */
#define _DEFAULT_SAMPLING_FEATURE (SENSOR_FEATURE_ACCELEROMETER)

/**
 * @brief Default sampling rate in "sampling" mode
 *
 * This is just an example to not spawn too much data to end user.
 */
#define _DEFAULT_SAMPLING_PERIOD_MS (300)
/**
 * @}
 */
// =============================| Defines - Tap |=============================
/**
 * @brief Tap related
 *
 * @{
 */

/**
 * @brief Default feature for tap detection
 */
#define _DEFAULT_TAP_FEATURE (SENSOR_FEATURE_ACCELEROMETER)

/**
 * @brief Default sampling rate
 *
 * Tap detection require quite high speed to work properly. So keep that in
 * mind
 */
#define _DEFAULT_TAP_SAMPLING_PERIOD_MS (4)

/**
 * @brief Define initial thresholds for tap detection
 *
 * @{
 */
#define _DEFAULT_TAP_MIN_THRESHOLD (500)
#define _DEFAULT_TAP_MAX_THRESHOLD (1700)
/**
 * @}
 */

/**
 * @brief Tap duration from accelerometer point of view
 *
 * This is kind of empiric value. Typically tap is quite short event which
 * takes few tens of ms.
 *
 * @{
 */
#define _DEFAULT_TAP_MIN_DURATION_MS (10)
#define _DEFAULT_TAP_MAX_DURATION_MS (40)
/**
 * @}
 */

/**
 * @brief If shock detected, but not tap, wait before detecting tap again
 */
#define _DEFAULT_TAP_SHOCK_HOLD_OFF_MS (800)
/**
 * @}
 */
// ====================| Defines - Orientation tracking |=====================
/**
 * @brief Orientation tracking related
 *
 * @{
 */

/**
 * @brief Default feature for orientation tracking
 */
#define _DEFAULT_ORIENTATION_FEATURE (SENSOR_FEATURE_ACCELEROMETER)

/**
 * @brief Default sampling rate for orientation detection
 */
#define _DEFAULT_ORIENTATION_SAMPLING_PERIOD_MS (100)

/**
 * @brief Default target position
 *
 * @{
 */
#define _DEFAULT_ORIENTATION_TARGET_THETA_DEG (90)
#define _DEFAULT_ORIENTATION_TARGET_PHI_DEG (90)
/**
 * @}
 */

/**
 * @brief Default orientation tolerance for target position
 *
 * @{
 */
#define _DEFAULT_ORIENTATION_TOLERANCE_THETA_DEG (40)
#define _DEFAULT_ORIENTATION_TOLERANCE_PHI_DEG (40)
/**
 * @}
 */

/**
 * @brief Default range when Phi should be ignored when estimate target
 *        position
 */
#define _DEFAULT_ORIENTATION_IGNORE_PHI_DEG (10)

/**
 * @{
 */
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  TAP_STATE_WAIT_FOR_SHOCK,               //!< TAP_STATE_WAIT_FOR_SHOCK
  TAP_STATE_CHECK_TAP_STATUS_END_PERIOD,  //!< TAP_STATE_CHECK_TAP_STATUS_END_PERIOD
  TAP_STATE_TAP_DETECTED,                 //!< TAP_STATE_TAP_DETECTED
  TAP_STATE_WAIT_FOR_CALM,                //!< TAP_STATE_WAIT_FOR_CALM
} te_tap_state;

/**
 * @brief Structure used by queue
 *
 * Data passed to main task basically
 */
typedef struct {
  ts_sensor_utils_data s_sensor_data;
  te_sensor_util_mode e_mode;
} ts_queue_data;

/**
 * @brief Structures used to keep arguments for every mode
 */
typedef struct {
  ts_sensor_util_sampling_args s_sampling;
  ts_sensor_util_tap_det_args s_tap_detection;
  ts_sensor_util_ori_det_args s_orientation_tracking;
} ts_mode_args;

/**
 * @brief Runtime variables for orientation tracking
 *
 * Not all variables are necessarily fit into argument structure. Some of them
 * are more process related
 */
typedef struct {
  // Orientation detection - need to store state of detection here, so it
  // can be reset when started again
  bool b_ori_tracking_prev_in_target_position;

  // Last measured Theta and Phi angles
  int16_t i16_last_theta;
  int16_t i16_lath_phi;
} ts_ori;

/**
 * @brief Set of parameters for "read raw, push to queue" function
 */
typedef struct {
  // Tell from which feature should be data read
  te_sensor_feature e_feature;

  // Tell to which mode data belongs
  te_sensor_util_mode e_mode;
} ts_read_raw_push_to_queue_param;

typedef struct {
  // Tells if task was initialized and running (so it is ready receive requests)
  // or not
  bool b_initialized;

  // Every mode (except stop) have own timer. So let's create simply list
  // for technically all modes.
  esp_timer_handle_t ah_timer[SENSOR_UTIL_MODE_MAX];

  // Queue for passing sensor data to main task
  QueueHandle_t h_sensor_data_queue;

  // Every mode have own argument structure, which should be kept somewhere,
  // since some variables might be required for post-processing
  ts_mode_args s_args;

  // Runtime variables for orientation tracking mode
  ts_ori s_ori;
} ts_sensor_utils_runtime;

// ===========================| Global variables |============================
static ts_sensor_utils_runtime ms_runtime = {
    .b_initialized = false,
};

static const char *tag = "Sensor utils";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Exit main task gracefully
 */
static void _exit_task(void);

/**
 * @brief Processing for tap detection
 * @param psSensorData Raw data from sensor
 */
static void _tap_detection(ts_sensor_utils_data *ps_sensor_data);

/**
 * @brief Processing for orientation tracking
 * @param psSensorData Raw data from sensor
 */
static void _oritantation_tracking(ts_sensor_utils_data *ps_sensor_data);
// ==============| Internal function prototypes: middle level |===============
/**
 * @brief Callback for timer which read raw data and push to main task
 * @param pvArgs Passed arguments. It is expected
 *        @ref tsReadRawPushToQueueParam structure, but due to generic nature
 *        of timer, it is pointer to void
 */
static void _timer_read_raw_push_to_queue_cb(void *pv_args);

/**
 * @brief Get Theta and Phi from integer vector structure
 *
 * @param sData Input vector data
 * @param[out] pi16thethaDeg Ouptut Theta angle in degrees
 * @param[out] pi16phiDeg Output Phi angle in degrees
 */
static void _get_theta_phi_int(ts_sensor_utils_data s_data,
                               int16_t *pi16_theta_deg, int16_t *pi16_phi_deg);

/**
 * @brief Get Theta and Phi from float vector structure
 *
 * @param sData Input vector data
 * @param[out] pi16thethaDeg Ouptut Theta angle in degrees
 * @param[out] pi16phiDeg Output Phi angle in degrees
 */
static void _get_theta_phi_float(ts_sensor_utils_data s_data,
                                 int16_t *pi16_thetha_deg,
                                 int16_t *pi16_phi_deg);
// ================| Internal function prototypes: low level |================
/**
 * @brief Pre-check routine for most of services
 *
 * Most of services checking same things, so idea is to simply recycle this
 * part of functionality.
 *
 * @param eFeature Selected feature
 * @param eMode Current mode
 * @param u32samplingPeriodMs Sampling period in milliseconds
 * @param pfCallback Pointer to callback function
 * @return SENSOR_OK is parameters looking good.
 */
static e_sensor_error _start_service_pre_check(
    const te_sensor_feature e_feature, const te_sensor_util_mode e_mode,
    const uint32_t u32_sampling_period_ms, const void *pf_callback);

/**
 * @brief Create and start timer machinery
 * @param[in] psTimerCfg Pointer to configuration structure
 * @param[out] phTimer Timer handler will be written here
 * @param u32periodMs Timer period in milliseconds
 * @return SENSOR_OK if no error
 */
static e_sensor_error _create_and_start_timer(
    const esp_timer_create_args_t *ps_timer_cfg, esp_timer_handle_t *ph_timer,
    uint32_t u32_period_ms);

/**
 * @brief Stop and delete timer
 * @param[in, out] phTimer Pointer to timer handler. Will be cleaned
 * @return SENSOR_OK if no error
 */
static e_sensor_error _stop_and_delete_timer(esp_timer_handle_t *ph_timer);

/**
 * @brief Read raw data from required feature and push it to queue
 *
 * @param[in] psParams Parameter structure. It contain feature and required
 *            mode
 */
static void _read_raw_push_to_queue(
    const ts_read_raw_push_to_queue_param *ps_params);
// =========================| High level functions |==========================
void sensors_utils_task(void *pv_args) {
  ms_runtime.b_initialized = false;

  // Initialize sensor logic if not initialized yet
  if (!sensors_are_initialized()) {
    // If there is any error, there is nothing to do actually
    if (sensors_initialize()) {
      _exit_task();
    }
  }

  // Create queue

  // Holds sampled data
  ts_queue_data s_queue_data;

  ms_runtime.h_sensor_data_queue =
      xQueueCreate(_QUEUE_NUM_OF_ITEMS, sizeof(s_queue_data));

  // Now it should be ready to receive requests
  ms_runtime.b_initialized = true;

  bool b_received;
  while (1) {
    // Wait (non-blocking) for data to be processed
    b_received = xQueueReceive(ms_runtime.h_sensor_data_queue, &s_queue_data,
                               portMAX_DELAY);

    if (b_received) {
      switch (s_queue_data.e_mode) {
        case SENSOR_UTIL_MODE_RAW_SAMPLES:
          // Not much to do, just pass data to callback
          assert(ms_runtime.s_args.s_sampling.pf_get_data);
          ms_runtime.s_args.s_sampling.pf_get_data(&s_queue_data.s_sensor_data);
          break;

        case SENSOR_UTIL_MODE_TAP_DETECTION:
          _tap_detection(&s_queue_data.s_sensor_data);
          break;
        case SENSOR_UTIL_MODE_ORIENTATION_TRACKING:
          _oritantation_tracking(&s_queue_data.s_sensor_data);
          break;

        default:
          // Internal error this should not happen
          SENSORS_LOGE(tag, "Unknown mode: %d", s_queue_data.e_mode);
          assert(0);
      }
    } else {
      // This was just timeout
      ESP_LOGD(tag, "Queue timeout");
    }
  }
}

inline bool sensors_utils_is_initialized(void) {
  return ms_runtime.b_initialized;
}
// ========================| Middle level functions |=========================
e_sensor_error sensors_utils_sampling_start(
    const ts_sensor_util_sampling_args s_args) {
  // This is constant for whole function
  static ts_read_raw_push_to_queue_param s_params = {
      .e_mode = SENSOR_UTIL_MODE_RAW_SAMPLES};
  s_params.e_feature = ms_runtime.s_args.s_sampling.e_feature;

  // Common pre-check
  e_sensor_error e_err_code = _start_service_pre_check(
      s_args.e_feature, s_params.e_mode, s_args.u32_sampling_period_ms,
      (void *)s_args.pf_get_data);
  if (e_err_code) {
    return e_err_code;
  }

  // To make processing easier, work with pointer to timer
  esp_timer_handle_t *ph_timer = &ms_runtime.ah_timer[s_params.e_mode];

  // So far so good, so store settings
  ms_runtime.s_args.s_sampling = s_args;

  // Try to start timer
  const esp_timer_create_args_t s_timer_cfg = {
      .callback = _timer_read_raw_push_to_queue_cb,
      .name = "sampling",
      .arg = (void *)&s_params,
  };

  return _create_and_start_timer(&s_timer_cfg, ph_timer,
                                 s_args.u32_sampling_period_ms);
}

e_sensor_error sensors_utils_sampling_stop(void) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  // Only timer is required to stop and delete
  return _stop_and_delete_timer(
      &ms_runtime.ah_timer[SENSOR_UTIL_MODE_RAW_SAMPLES]);
}

e_sensor_error sensors_utils_tap_detection_start(
    const ts_sensor_util_tap_det_args s_args) {
  // This is constant for whole function
  static ts_read_raw_push_to_queue_param s_params = {
      .e_mode = SENSOR_UTIL_MODE_TAP_DETECTION};
  s_params.e_feature = ms_runtime.s_args.s_tap_detection.e_feature;

  // Common pre-check
  e_sensor_error e_err_code = _start_service_pre_check(
      s_args.e_feature, s_params.e_mode, s_args.u32_sampling_period_ms,
      (void *)s_args.pf_tap_detected);
  if (e_err_code) {
    return e_err_code;
  }

  // To make processing easier, work with pointer to timer
  esp_timer_handle_t *ph_timer = &ms_runtime.ah_timer[s_params.e_mode];

  // Store arguments
  ms_runtime.s_args.s_tap_detection = s_args;

  // Try to start timer
  const esp_timer_create_args_t s_timer_cfg = {
      .callback = _timer_read_raw_push_to_queue_cb,
      .name = "tap detection",
      .arg = (void *)&s_params,
  };

  return _create_and_start_timer(&s_timer_cfg, ph_timer,
                                 s_args.u32_sampling_period_ms);
}

e_sensor_error sensors_utils_tap_detection_stop(void) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  // Only timer is required to stop and delete
  return _stop_and_delete_timer(
      &ms_runtime.ah_timer[SENSOR_UTIL_MODE_TAP_DETECTION]);
}

e_sensor_error sensors_utils_ori_tracking_start(
    const ts_sensor_util_ori_det_args s_args) {
  // Assume, that device is not in required target position for now. it have to
  // be reset every time when service is started
  ms_runtime.s_ori.b_ori_tracking_prev_in_target_position = false;

  // This is constant for whole function
  static ts_read_raw_push_to_queue_param s_params = {
      .e_mode = SENSOR_UTIL_MODE_ORIENTATION_TRACKING};
  s_params.e_feature = ms_runtime.s_args.s_orientation_tracking.e_feature;

  // Common pre-check
  e_sensor_error e_err_code = _start_service_pre_check(
      s_args.e_feature, s_params.e_mode, s_args.u32_sampling_period_ms,
      (void *)s_args.pf_ori_changed);
  if (e_err_code) {
    return e_err_code;
  }

  // To make processing easier, work with pointer to timer
  esp_timer_handle_t *ph_timer = &ms_runtime.ah_timer[s_params.e_mode];

  // Store arguments
  ms_runtime.s_args.s_orientation_tracking = s_args;

  // Try to start timer
  const esp_timer_create_args_t s_timer_cfg = {
      .callback = _timer_read_raw_push_to_queue_cb,
      .name = "ori track",
      .arg = (void *)&s_params,
  };

  return _create_and_start_timer(&s_timer_cfg, ph_timer,
                                 s_args.u32_sampling_period_ms);
}

e_sensor_error sensors_utils_ori_tracking_stop(void) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  // Only timer is required to stop and delete
  return _stop_and_delete_timer(
      &ms_runtime.ah_timer[SENSOR_UTIL_MODE_ORIENTATION_TRACKING]);
}

e_sensor_error sensors_utils_ori_last_angles(int16_t *pi16_theta_deg,
                                             int16_t *pi16_phi_deg) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  if ((!pi16_theta_deg) || (!pi16_phi_deg)) {
    return SENSOR_EMPTY_POINTER;
  }

  *pi16_theta_deg = ms_runtime.s_ori.i16_last_theta;
  *pi16_phi_deg = ms_runtime.s_ori.i16_lath_phi;

  return SENSOR_OK;
}
// ==========================| Low level functions |==========================
e_sensor_error sensors_utils_get_ori_angles(const ts_sensor_utils_data *ps_data,
                                            int16_t *pi16_theta_deg,
                                            int16_t *pi16_phi_deg) {
  // All pointers have to be set
  if ((!ps_data) || (!pi16_theta_deg) || (!pi16_phi_deg)) {
    return SENSOR_EMPTY_POINTER;
  }

  // Check data type. It have to be vector. If not, then it is not possible to
  // calculate angles
  const te_sensor_data_type e_type = ps_data->e_data_type;
  if ((e_type != SENSOR_DTYPE_RAW_VECT) && (e_type != SENSOR_DTYPE_INT_VECT) &&
      (e_type != SENSOR_DTYPE_FLOAT_VECT)) {
    return SENSOR_INVALID_DATA_TYPE;
  }

  // In case of integer, it is possible to use lightweight mathematical
  // functions. However for float, we have to use "full" functions
  if (e_type == SENSOR_DTYPE_FLOAT_VECT) {
    _get_theta_phi_float(*ps_data, pi16_theta_deg, pi16_phi_deg);
  } else {
    // Vector is integer based
    _get_theta_phi_int(*ps_data, pi16_theta_deg, pi16_phi_deg);
  }

  return SENSOR_OK;
}

bool sensors_utils_ori_is_in_position(
    const int16_t i16_theta_deg, const int16_t i16_phi_deg,
    const ts_sensor_util_ori_det_params s_params) {
  bool b_is_in_target_position = false;

  bool b_is_theta_match = mf_is_angle_in_range(
      i16_theta_deg,
      s_params.i16_target_theta_deg - s_params.i16_tolerance_theta_deg / 2,
      s_params.i16_target_theta_deg + s_params.i16_tolerance_theta_deg / 2);

  // Decide whether Phi angle should be compared or not
  bool b_do_not_use_phi = false;

  // In case that Thetha is 0 or 180, it does not make sense to evaluate if Phi
  // is in required range, since due to mathematics, it will simply oscillate
  // wildly around those values.
  // However in real life, more wide range should be checked, because of
  // inaccuracy of components.
  // Note that Thetha can be in range 0~180 degrees.
  if ((s_params.i16_target_theta_deg <=
       s_params.u16_ignore_phi_tolerance_deg) ||
      (s_params.i16_target_theta_deg >=
       (180 - s_params.u16_ignore_phi_tolerance_deg))) {
    b_do_not_use_phi = true;
  }

  if (b_do_not_use_phi) {
    // No need to check more. Just check if Thetha angle is in range
    b_is_in_target_position = b_is_theta_match;

  } else if (b_is_theta_match) {
    // Investigate about Phi only if Thetha match
    bool b_is_phi_match = mf_is_angle_in_range(
        i16_phi_deg,
        s_params.i16_target_phi_deg - s_params.i16_tolerance_phi_deg / 2,
        s_params.i16_target_phi_deg + s_params.i16_tolerance_phi_deg / 2);

    if (b_is_theta_match && b_is_phi_match) {
      b_is_in_target_position = true;

    } else {
      b_is_in_target_position = false;
    }

  } else {
    // Not in target position
    b_is_in_target_position = false;
  }

  return b_is_in_target_position;
}
// ======================| Get default argument values |======================
void sensors_utils_get_default_sampling_args(
    ts_sensor_util_sampling_args *ps_args) {
  // Pointer should not be empty
  if (!ps_args) {
    assert(0);
    return;
  }

  ps_args->e_feature = _DEFAULT_SAMPLING_FEATURE;
  ps_args->u32_sampling_period_ms = _DEFAULT_SAMPLING_PERIOD_MS;
  // Keep callback unchanged - there is no default
}

void sensors_utils_get_default_tap_detect(
    ts_sensor_util_tap_det_args *ps_args) {
  // Pointer should not be empty
  if (!ps_args) {
    assert(0);
    return;
  }

  ps_args->e_feature = _DEFAULT_TAP_FEATURE;
  ps_args->u32_sampling_period_ms = _DEFAULT_TAP_SAMPLING_PERIOD_MS;
  // Keep callback unchanged - there is no default

  ps_args->s_params.u16_min_vector_length = _DEFAULT_TAP_MIN_THRESHOLD;
  ps_args->s_params.u16_max_vector_length = _DEFAULT_TAP_MAX_THRESHOLD;

  ps_args->s_params.u16_min_duration_ms = _DEFAULT_TAP_MIN_DURATION_MS;
  ps_args->s_params.u16_max_duration_ms = _DEFAULT_TAP_MAX_DURATION_MS;

  ps_args->s_params.u16_hold_off_ms = _DEFAULT_TAP_SHOCK_HOLD_OFF_MS;
}

void sensors_utils_get_default_ori_tracking(
    ts_sensor_util_ori_det_args *ps_args) {
  // Pointer should not be empty
  if (!ps_args) {
    assert(0);
    return;
  }

  ps_args->e_feature = _DEFAULT_ORIENTATION_FEATURE;
  ps_args->u32_sampling_period_ms = _DEFAULT_ORIENTATION_SAMPLING_PERIOD_MS;
  // Keep callback unchanged - there is no default

  ps_args->s_params.i16_target_theta_deg =
      _DEFAULT_ORIENTATION_TARGET_THETA_DEG;
  ps_args->s_params.i16_target_phi_deg = _DEFAULT_ORIENTATION_TARGET_PHI_DEG;

  ps_args->s_params.i16_tolerance_theta_deg =
      _DEFAULT_ORIENTATION_TOLERANCE_THETA_DEG;
  ps_args->s_params.i16_tolerance_phi_deg =
      _DEFAULT_ORIENTATION_TOLERANCE_PHI_DEG;

  ps_args->s_params.u16_ignore_phi_tolerance_deg =
      _DEFAULT_ORIENTATION_IGNORE_PHI_DEG;
}
// ====================| Internal functions: high level |=====================
static void _exit_task(void) {
  SENSORS_LOGI(tag, "Exiting task");

  ms_runtime.b_initialized = false;

  // Clean up - release resources
  for (te_sensor_util_mode e_mode = (te_sensor_util_mode)0;
       e_mode < SENSOR_UTIL_MODE_MAX; e_mode++) {
    // If handler is not empty, stop and delete it
    if (ms_runtime.ah_timer[e_mode]) {
      esp_timer_stop(ms_runtime.ah_timer[e_mode]);
      esp_timer_delete(ms_runtime.ah_timer[e_mode]);

      // Clean up pointer to handler
      ms_runtime.ah_timer[e_mode] = 0;
    }
  }

  vQueueDelete(ms_runtime.h_sensor_data_queue);

  // Zero means: delete own task
  vTaskDelete(0);
}

static void _tap_detection(ts_sensor_utils_data *ps_sensor_data) {
  static int32_t i32_previous_vect_len = 0;

  // Keep track when shock was detected
  static uint32_t u32_shock_detected_ms = 0;

  // Log when calm is detected. Zero means "not yet"
  static uint32_t u32_calm_detected_ms = 0;

  // Variable for simple state machine
  static te_tap_state e_state = TAP_STATE_WAIT_FOR_SHOCK;

  int32_t i32_current_vector_len;
  int32_t i32_current_vector_diff_abs;

  uint32_t u32_current_time_ms = GET_CLK_MS_32BIT();

  assert((ps_sensor_data->e_data_type == SENSOR_DTYPE_RAW_VECT) ||
         (ps_sensor_data->e_data_type == SENSOR_DTYPE_INT_VECT));

  // Convenient way to get to the parameters
  const ts_sensor_util_tap_det_args *ps_cfg =
      &ms_runtime.s_args.s_tap_detection;

  // Convert input data to vector accepted by math modules
  ts_mv_vector_16 s_vector_16 = {
      .i16_x = ps_sensor_data->u_d.s_vect.i16_x,
      .i16_y = ps_sensor_data->u_d.s_vect.i16_y,
      .i16_z = ps_sensor_data->u_d.s_vect.i16_z,
  };
  mv_vect_16_length(&s_vector_16, &i32_current_vector_len);

  // If previous vector length was never set (zero), set it
  if (i32_previous_vect_len == 0) {
    i32_previous_vect_len = i32_current_vector_len;
  }

  // Need to get absolute value
  i32_current_vector_diff_abs = i32_current_vector_len - i32_previous_vect_len;
  if (i32_current_vector_diff_abs < 0) {
    i32_current_vector_diff_abs *= -1;
  }

  switch (e_state) {
    case TAP_STATE_WAIT_FOR_SHOCK:
      if ((i32_current_vector_diff_abs >
           ps_cfg->s_params.u16_min_vector_length) &&
          (i32_current_vector_diff_abs <
           ps_cfg->s_params.u16_max_vector_length)) {
        u32_shock_detected_ms = u32_current_time_ms;

        e_state = TAP_STATE_CHECK_TAP_STATUS_END_PERIOD;

      } else {
        // Stay in this mode
      }

      break;
    case TAP_STATE_CHECK_TAP_STATUS_END_PERIOD:
      // Now waiting for end of tap
      if (i32_current_vector_diff_abs >
          ps_cfg->s_params.u16_max_vector_length) {
        // Shock is too big. Let's wait for calm state
        e_state = TAP_STATE_WAIT_FOR_CALM;

      } else if (u32_current_time_ms < (u32_shock_detected_ms +
                                        ps_cfg->s_params.u16_max_duration_ms)) {
        // If within timeout above, accelerometer get under threshold, it means
        // that tap is detected

        // After first shock, accelerometer should be "calm"
        if ((i32_current_vector_diff_abs <
             ps_cfg->s_params.u16_min_vector_length) &&
            (u32_current_time_ms >
             (u32_shock_detected_ms + ps_cfg->s_params.u16_min_duration_ms))) {
          e_state = TAP_STATE_TAP_DETECTED;
        } else {
          // Not sure if tap or not. Try again later until have time
        }
      } else {
        // Too long for tap event. Maybe earthquake?
        e_state = TAP_STATE_WAIT_FOR_CALM;
      }
      break;

    case TAP_STATE_WAIT_FOR_CALM:
      if (i32_current_vector_diff_abs <
          ps_cfg->s_params.u16_min_vector_length) {
        // Calm

        // If calm situation was not registered yet, now is the time
        if (u32_calm_detected_ms == 0) {
          u32_calm_detected_ms = u32_current_time_ms;

        } else if ((u32_current_time_ms - u32_calm_detected_ms) >
                   ps_cfg->s_params.u16_hold_off_ms) {
          // This is not tap, because shock was too long. Wait for another
          // shock
          e_state = TAP_STATE_WAIT_FOR_SHOCK;
        } else {
          // Calm now, but need to wait bit more
        }

      } else {
        // Still something happen. Shock is registered. Reset "calm" timer
        u32_calm_detected_ms = 0;
      }
      break;

    case TAP_STATE_TAP_DETECTED:
      // Tap detected -> execute callback
      ps_cfg->pf_tap_detected();

      e_state = TAP_STATE_WAIT_FOR_SHOCK;
      break;

    default:
      // This should not happen
      ESP_LOGE(tag, "Tap detection: Invalid state: %d", e_state);
      // Move to initial state. Kind of self fix
      e_state = TAP_STATE_WAIT_FOR_SHOCK;
  }

  i32_previous_vect_len = i32_current_vector_len;
}

static void _oritantation_tracking(ts_sensor_utils_data *ps_sensor_data) {
  int16_t i16_theta, i16_phi;

  e_sensor_error e_err_code =
      sensors_utils_get_ori_angles(ps_sensor_data, &i16_theta, &i16_phi);
  if (e_err_code) {
    SENSORS_LOGE(tag, "Calculating Thetha and Phi failed: %s",
                 sensor_error_code_to_str(e_err_code));
    return;
  }

  // Theta and Phi are correctly calculated -> store them
  ms_runtime.s_ori.i16_last_theta = i16_theta;
  ms_runtime.s_ori.i16_lath_phi = i16_phi;

  bool b_is_in_target_position = sensors_utils_ori_is_in_position(
      i16_theta, i16_phi, ms_runtime.s_args.s_orientation_tracking.s_params);

  // Previous and actual value differs
  if (b_is_in_target_position !=
      ms_runtime.s_ori.b_ori_tracking_prev_in_target_position) {
    // There is change -> execute callback
    ms_runtime.s_args.s_orientation_tracking.pf_ori_changed(
        b_is_in_target_position);
  }

  // Store actual status for next time
  ms_runtime.s_ori.b_ori_tracking_prev_in_target_position =
      b_is_in_target_position;
}
// ===================| Internal functions: middle level |====================
static void _timer_read_raw_push_to_queue_cb(void *pv_args) {
  // Just retype pointer to specific one
  _read_raw_push_to_queue((ts_read_raw_push_to_queue_param *)pv_args);
}

static void _get_theta_phi_int(ts_sensor_utils_data s_data,
                               int16_t *pi16_theta_deg, int16_t *pi16_phi_deg) {
  /* Theory: there are 3 equations which describe theta and phi angle
   *
   * acc_x =  1G * sin(theta) * cos(phi)
   * acc_y = -1G * sin(theta) * sin(phi)
   * acc_z =  1G * cos(theta)
   *
   * The theta describe how much is device divergent from 1G axe (down). The
   * theta can be from 0~180 degrees and can not be negative number. It is only
   * related to 1G axe.
   *
   * The phi tells how much is accelerometer rotated relative to it's Z axe.
   * Practically this value will be quite unstable when theta is around 0 or
   * 180 (noise value actually do most of damage). Basically phi does not have
   * much meaning if only accelerometer data are used in those 2 cases.
   *
   * By doing some math, we get following equations for getting theta and phi:
   *
   * theta = acos( acc_z / 1G )
   * phi = atan( -1 * acc_y / acc_x )
   *
   */

  // Vector length
  int32_t i32_vector_len;

  // Does not use 1G as hard-coded constant. Assume that vector size is 1G,
  // since this mode is dealing with orientation.

  // Need to convert input to math-like vector
  ts_mv_vector_16 s_vector_16 = {
      .i16_x = s_data.u_d.s_vect.i16_x,
      .i16_y = s_data.u_d.s_vect.i16_y,
      .i16_z = s_data.u_d.s_vect.i16_z,
  };
  mv_vect_16_length(&s_vector_16, &i32_vector_len);

  // It is expected that vector size is not zero. However it is accelerometer,
  // so if there would be such external force, which would goes against gravity
  // exactly same, result could be zero. The we would deal problem with
  // division by zero and reset. Better to avoid that
  if (i32_vector_len == 0) {
    i32_vector_len = 1;
  }

  // The "fast" functions work with multiplier of 1e6. Need to work with 64 bit
  // long numbers, due to 1e6 multiplication
  int32_t i32_theta_deg = mf_acos(
      ((int64_t)1000000 * (int64_t)s_data.u_d.s_vect.i16_z) / i32_vector_len);

  int32_t i32_phi_deg;

  // Deal with zero division
  if (s_data.u_d.s_vect.i16_x == 0) {
    i32_phi_deg = mf_atan(INT32_MAX);

  } else {
    i32_phi_deg =
        mf_atan(((int64_t)-1000000 * (int64_t)s_data.u_d.s_vect.i16_y) /
                s_data.u_d.s_vect.i16_x);
  }

  // The arctan() can not from 1 number estimate to which quadrant was value
  // referred to. Basically we need to do angle correction.
  if ((s_data.u_d.s_vect.i16_x < 0) && (s_data.u_d.s_vect.i16_y < 0)) {
    i32_phi_deg += 180;
  } else if ((s_data.u_d.s_vect.i16_x < 0) && (s_data.u_d.s_vect.i16_y >= 0)) {
    i32_phi_deg -= 180;
  } else {
    // +X && -Y or +X && +Y
    // Actually no need to do correction
  }

  // Everything is cool, let's write Thetha and Phi to pointers
  *pi16_theta_deg = i32_theta_deg;
  *pi16_phi_deg = i32_phi_deg;
}

static void _get_theta_phi_float(ts_sensor_utils_data s_data,
                                 int16_t *pi16_thetha_deg,
                                 int16_t *pi16_phi_deg) {
  // For theory and detailed explanation, refer to function _getThetaPhiInt.
  // Here is just math

  // Simplify access to the vector
  ts_sensor_vect_float s_vect = s_data.u_d.s_vect_float;

  // Calculating length: sqrt(x*x + y*y + z*z)
  float f_vect_len =
      sqrtf((s_vect.f_x * s_vect.f_x) + (s_vect.f_y * s_vect.f_y) +
            (s_vect.f_z * s_vect.f_z));

  // Value should not be zero. But, data are from external sensor. You never
  // know, so to avoid issue with dividing zero, here is small hack
  if (f_vect_len == 0) {
    f_vect_len = 0.0001;
  }

  float f_thetha_rad = acosf(s_vect.f_z / f_vect_len);

  // Deal with zero division
  if (s_vect.f_x == 0) {
    s_vect.f_x = 0.0001;
  }

  float f_phi_rad = atan2f(s_vect.f_y, s_vect.f_x);

  // Recalculate radian to degrees
  float p_theta_deg = (180 * f_thetha_rad) / M_PI;
  float p_phi_deg = (180 * f_phi_rad) / M_PI;

  *pi16_thetha_deg = (int16_t)p_theta_deg;
  *pi16_phi_deg = (int16_t)p_phi_deg;
}
// =====================| Internal functions: low level |=====================
static e_sensor_error _start_service_pre_check(
    const te_sensor_feature e_feature, const te_sensor_util_mode e_mode,
    const uint32_t u32_sampling_period_ms, const void *pf_callback) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  // To make processing easier, work with pointer to timer
  esp_timer_handle_t *ph_timer = &ms_runtime.ah_timer[e_mode];

  // If timer is already set, it means this more is already running
  if (*ph_timer) {
    return SENSOR_ALREADY_RUNNING;
  }

  // Process arguments. Feature have to be within supported range, callback
  // have to be set, sampling period have to be non-zero value
  if ((e_feature >= SENSOR_FEATURE_MAX) || (pf_callback == 0) ||
      (u32_sampling_period_ms == 0)) {
    return SENSOR_INVALID_PARAM;
  }

  // If feature is not available, do not even try to run anything
  if (!sensor_is_available(e_feature)) {
    return SENSOR_NOT_SUPPORTED;
  }

  // Otherwise it looking good
  return SENSOR_OK;
}

static e_sensor_error _create_and_start_timer(
    const esp_timer_create_args_t *ps_timer_cfg, esp_timer_handle_t *ph_timer,
    uint32_t u32_period_ms) {
  if (esp_timer_create(ps_timer_cfg, ph_timer)) {
    return SENSOR_TIMER_INITIALIZATION_FAILED;
  }

  uint64_t u64_sampling_period_us = 1000UL * (uint64_t)u32_period_ms;

  if (esp_timer_start_periodic(*ph_timer, u64_sampling_period_us)) {
    return SENSOR_TIMER_INITIALIZATION_FAILED;
  } else {
    return SENSOR_OK;
  }
}

static e_sensor_error _stop_and_delete_timer(esp_timer_handle_t *ph_timer) {
  // If timer is not set, it means it was not started before
  if (!ph_timer) {
    return SENSOR_NOT_RUNNING;
  }

  // Stop timer and clean up handler
  e_sensor_error e_err_code = SENSOR_OK;

  if (esp_timer_stop(*ph_timer)) {
    e_err_code = SENSOR_TIMER_DEINITIALIZATION_FAILED;
  }

  if (esp_timer_delete(*ph_timer)) {
    e_err_code = SENSOR_TIMER_DEINITIALIZATION_FAILED;
  }

  // Cleanup timer either way - we do not want to use it
  *ph_timer = 0;

  return e_err_code;
}

static void _read_raw_push_to_queue(
    const ts_read_raw_push_to_queue_param *ps_params) {
  ts_queue_data s_queue_data = {.e_mode = ps_params->e_mode};

  // Load raw data and sent if no error detected
  e_sensor_error e_err_code =
      sensor_get_raw(ps_params->e_feature, &s_queue_data.s_sensor_data);

  if (e_err_code) {
    SENSORS_LOGE(tag, "Reading raw data failed: %s (Feature: %s @ %d mode)",
                 sensor_error_code_to_str(e_err_code),
                 sensor_feature_to_str(ps_params->e_feature),
                 s_queue_data.e_mode);
  } else {
    // Push data to queue
    xQueueSend(ms_runtime.h_sensor_data_queue, &s_queue_data, portMAX_DELAY);
  }
}
